from sympy.solvers import solve
from sympy import Symbol


def calc():
    def start():
        answer = input("Wanna start again? [y/n]")
        if answer.lower() == 'y' or answer.lower() == 'yes':
            calc()
        else:
            print("Bye!")

    def grappa():
        try:
            return float(input('Enter amount of alcohol in Grappa [%]:')) * 0.01
        except ValueError:
            print("Must be numbers only!")
            grappa()

    def musk():
        try:
            return float(input('Enter the desired amount of Shorto [ml]:'))
        except ValueError:
            print("Must be numbers only!")
            musk()

    grapp = grappa()
    cml = musk()
    bml = Symbol('x')

    eq = solve(bml * (grapp - 0.07) - (0.12 * cml), bml)
    bml = round(eq[0], 1)
    aml = cml - bml

    if round((aml * 0.07 + bml * grapp), 0) == round((cml * 0.19), 0) and (aml + bml) == cml:
        print("Grape musk: %.0f ml" % aml)
        print("Grappa: %.0f ml" % bml)
        print("Enjoy!")
        start()
    else:
        print("Something went wrong!")
        start()


if __name__ == '__main__':
    calc()
