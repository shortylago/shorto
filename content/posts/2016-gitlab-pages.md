Title: Very easy way to mix proper Port wine!
Date: 2019-01-08
Category: GitLab
Tags: porto
Slug: shorto

**_`All you need is measuring cup, grape must and grapa.`_**

```python
from sympy.solvers import solve
from sympy import Symbol


def calc():
    def grappa():
        try:
            return float(input('Enter amount of alcohol in Grappa [%]:')) * 0.01
        except ValueError:
            print("Must be numbers only!")
            grappa()

    def musk():
        try:
            return float(input('Enter the desired amount of Shorto [ml]:'))
        except ValueError:
            print("Must be numbers only!")
            musk()

    grapp = grappa()
    cml = musk()
    bml = Symbol('x')

    eq = solve(bml * (grapp - 0.07) - (0.12 * cml), bml)
    bml = round(eq[0], 1)
    aml = cml - bml

    if (aml * 0.07 + bml * grapp) == (cml * 0.19) and (aml + bml) == cml:
        print("Grape musk: %.0f ml" % aml)
        print("Grappa: %.0f ml" % bml)
    else:
        print("Something went wrong!")
        answer = input("Wanna try again? [y/n]")
        if answer.lower() == 'y' or answer.lower() == 'yes':
            calc()


if __name__ == '__main__':
    calc()
```